package test.data_structures;
import model.data_structures.Node;
import junit.framework.TestCase;

public class TestNode extends TestCase {
	
	private Node nodo;
	private Node nodo2;
	private Node nodoaux;
	private void setupEscenario1()
	{
		nodo = new Node (1,nodo2);
	}
	
	
	public void testConstructor()
	{
		setupEscenario1();
		assertEquals("Error al inicializar el item del nodo",1,nodo.getItem());
		assertEquals("Error al inicializar el nodo siguiente al nodo actual", nodo2,nodo.getNext());
	}
	
	public void testGetItem()
	{
		setupEscenario1();
		assertEquals("Error al obtener el item de el nodo actual",1,nodo.getItem());
	}
	
	public void testSetItem()
	{
		setupEscenario1();
		int itemPrueba = 2;
		nodo.setItem(itemPrueba);
		assertEquals("Error al cambiar el Item del nodo actual",itemPrueba, nodo.getItem());
	}
	
	public void testGetNext()
	{
		setupEscenario1();
		assertEquals("Error al obtener el nodo siguiente al actual",nodo2,nodo.getNext());
	}
	
	public void testSetNext()
	{
		setupEscenario1();
		testGetNext();
		Node nodo3 = new Node(3,nodoaux);
		nodo.setNext(nodo3);
		assertEquals("Error al cambiar el nodo siguiente al actual",nodo3,nodo.getNext());
	}
	
}
