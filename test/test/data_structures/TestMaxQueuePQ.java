package test.data_structures;

import model.data_structures.MaxQueuePQ;
import model.logic.TravelTime;
import org.junit.Before;
import org.junit.Test;
import controller.Viaje;
import static org.junit.Assert.assertEquals;

public class TestMaxQueuePQ {	
	
	private MaxQueuePQ<TravelTime> listaTravel;
	
	private MaxQueuePQ<Integer> listaInteger;
	
	private TravelTime[] listaTravelOrdenada={new TravelTime(0,0,0,0,10,0), new TravelTime(0,0,0,0,20,0), new TravelTime(0,0,0,0,30,0), new TravelTime(0,0,0,0,40,0), new TravelTime(0,0,0,0,50,0)};
	
	private Integer[] listaIntegerOrdenada={0,1,2,3,4,5,6,7,8,9};
	
	private TravelTime[] listaTravelDesordenada={new TravelTime(0,0,0,0,20,0), new TravelTime(0,0,0,0,10,0), new TravelTime(0,0,0,0,50,0), new TravelTime(0,0,0,0,30,0), new TravelTime(0,0,0,0,40,0)};
	
	private Integer[] listaIntegerDesordenada={8,3,2,1,4,9,6,7,0};
	
	@Before
	public void setupEscenario1() {
		listaTravel=new MaxQueuePQ<TravelTime>();
		for(Integer i=0; i<listaTravelOrdenada.length; i++){
			listaTravel.insert(listaTravelOrdenada[i]);
		}
		listaInteger=new MaxQueuePQ<Integer>();
		for(Integer i=0; i<listaIntegerOrdenada.length; i++){
			listaInteger.insert(listaIntegerOrdenada[i]);
		}
	}

	@Before
	public void setupEscenario2() {
		listaTravel=new MaxQueuePQ<TravelTime>();
		for(Integer i=0; i<listaTravelDesordenada.length; i++){
			listaTravel.insert(listaTravelDesordenada[i]);
		}
		listaInteger=new MaxQueuePQ<Integer>();
		for(Integer i=0; i<listaIntegerDesordenada.length; i++){
			listaInteger.insert(listaIntegerDesordenada[i]);
		}
	}

	@Test
	public void testIsEmpty()
	{
		listaInteger=new MaxQueuePQ<Integer>();
		listaTravel=new MaxQueuePQ<TravelTime>();
		assertEquals(true, listaTravel.isEmpty());
		assertEquals(true, listaInteger.isEmpty());
	}
	
	@Test
	public void testMax1() {
		setupEscenario1();
		assertEquals(0, listaTravel.max().compareTo(listaTravelOrdenada[listaTravelOrdenada.length-1]));
		assertEquals(true, listaInteger.max()==listaIntegerOrdenada[listaIntegerOrdenada.length-1]);
	}
	
	@Test
	public void testMax2() {
		setupEscenario2();
		assertEquals(0, listaTravel.max().compareTo(listaTravelDesordenada[2]));
		assertEquals(true, listaInteger.max()==listaIntegerDesordenada[5]);
	}
	
	@Test
	public void testSize1() {
		setupEscenario1();
		assertEquals(listaTravelOrdenada.length,listaTravel.size());
		assertEquals(listaIntegerOrdenada.length,listaInteger.size());
	}
	
	@Test
	public void testSize2() {
		setupEscenario2();
		assertEquals(listaTravelDesordenada.length,listaTravel.size());
		assertEquals(listaIntegerDesordenada.length,listaInteger.size());
	}
	
	@Test
	public void testDelMax1() 
	{
		setupEscenario1();
		assertEquals(0, listaTravel.delMax().compareTo(listaTravelOrdenada[listaTravelOrdenada.length-1]));
		assertEquals(true, listaInteger.delMax()==listaIntegerOrdenada[listaIntegerOrdenada.length-1]);
		assertEquals(listaTravelOrdenada.length-1,listaTravel.size());
		assertEquals(listaIntegerOrdenada.length-1,listaInteger.size());
	}
	
	@Test
	public void testDelMax2() 
	{
		setupEscenario2();
		assertEquals(0, listaTravel.delMax().compareTo(listaTravelDesordenada[2]));
		assertEquals(true, listaInteger.delMax()==listaIntegerDesordenada[5]);
		assertEquals(listaTravelDesordenada.length-1,listaTravel.size());
		assertEquals(listaIntegerDesordenada.length-1,listaInteger.size());
	}

}
