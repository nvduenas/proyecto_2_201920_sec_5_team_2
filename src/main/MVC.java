package main;

import controller.Controller;

import java.io.IOException;

public class MVC {
	
	public static void main(String[] args) 
	{
		Controller controler = new Controller();
		try {
			controler.run();
		} catch (IOException e) {
			e.printStackTrace();
		}

	}
}
