package controller;

import model.data_structures.TwoWayList;
import model.logic.*;

import view.MVCView;

import java.io.IOException;
import java.util.Iterator;
import java.util.Scanner;

public class Controller {

	/* Instancia del Modelo*/
	private MVCModelo modelo;

	/* Instancia de la Vista*/
	private MVCView view;

	/**
	 * Crear la vista y el modelo del proyecto
	 */
	public Controller() {
		view = new MVCView();
		modelo = new MVCModelo();
	}

	public void run() throws IOException {
		Scanner lector = new Scanner(System.in);
		boolean fin = false;
		String dato = "";
		String respuesta = "";
		long startTime = 0;
		long endTime = 0; // medici�n tiempo actual
		long duration = 0; // duracion de ejecucion del algoritmo
		while (!fin) {

			view.printMenu();

			int option = lector.nextInt();
			switch (option) {
				case 1:
					startTime=System.currentTimeMillis();
					modelo.load();
				    System.out.println("Travel time loaded");
				    endTime = System.currentTimeMillis();
				    duration = endTime - startTime; // duracion de ejecucion del algoritmo
                    System.out.println("\n----------------\n Load time: " + duration / 1000 + " seconds" + "\n----------------\n");
                    System.out.println("Total traveltime registered: " + modelo.darViajesHourly().size() + "---------\n" +
                            "Min id zone: " + modelo.darMenor() + "---------\n" +
                            "Max id zone: " + modelo.darMayor() + "---------\n");
                    break;
				case 2:
					System.out.println("Give the N zones nearest to the north\n");
					int nZones=lector.nextInt();
					Malla[]ml=modelo.giveNorthZones(nZones);
					for (int i = 0; i < ml.length; i++) {
						System.out.println("Zone id"+ml[i].darId()+";Longitude"+ml[i].darLongitud()+";Latitude"+ml[i].darLatitud());
					}
					break;
				case 3:
					System.out.println("Give longitude and latitude for finding the neighbour's zones\n latitude");
					double lati=Double.parseDouble(lector.next());
					System.out.println("longitude");
					double longi= Double.parseDouble(lector.next());
					TwoWayList<Malla>br=modelo.neighborLocations(lati,longi);
					Iterator<Malla>itr=br.iterator();
					Malla mali=null;
					while((mali=itr.next())!=null)
					{
						System.out.println("Zone id"+mali.darId()+";Longitude"+mali.darLongitud()+";Latitude"+mali.darLatitud());
					}
					break;
				case 4:
					System.out.println("Standard deviation limits\nfloor");
					double floor=lector.nextDouble();
					System.out.println("roof");
					double roof=lector.nextDouble();
					TwoWayList<UBERTrip>ut=modelo.limStdDev(floor,roof);
					Iterator<UBERTrip>it=ut.iterator();
					UBERTrip ub=null;
					while((ub=it.next())!=null)
					{
						System.out.println("Source id "+ub.give_sourceid()+";dstid"+ub.givedstid()+";month"+ub.give_date()+";std_dev "+ub.give_standard_deviation_travel_time());
					}
					break;
				case 5:
					System.out.println("Mean travel time by a dstid and an hour\n dstid");
					int dstid=lector.nextInt();
					System.out.println("hour");
					int hour=lector.nextInt();
					TwoWayList<TravelTime>tl=modelo.getTravelTimesByGivenZoneHour(dstid,hour);
					Iterator<TravelTime>irl=tl.iterator();
					TravelTime time=null;
					while((time=irl.next())!=null)
					{
						System.out.println("Source id "+time.give_sourceid()+";dstid"+time.give_distid()+";hour"+time.give_hod()+";mean travel "+time.give_mean_travel_time());
					}
					break;
				case 6:
					System.out.println("Mean travel time by a dstid and range hour\n dstid");
					int distid=lector.nextInt();
					System.out.println("floor");
					int floorHour=lector.nextInt();
					System.out.println("roof");
					int roofHour=lector.nextInt();
					TravelTime[] t1=modelo.getTravelTimesByGivenZoneRangeHour(distid,floorHour,roofHour);
					for (int i = 0; i < t1.length; i++) {
						System.out.println("Source id "+t1[i].give_sourceid()+";dstid"+t1[i].give_distid()+";hour"+t1[i].give_hod()+";mean travel "+t1[i].give_mean_travel_time());

					}
					break;
				case 7:
					System.out.println("N elements with max zones delimitators");
					int nelm=lector.nextInt();
					UberZone[]uzones=modelo.priorityZonesByNum(nelm);
					for (int i = 0; i < uzones.length; i++) {
						System.out.println(uzones[i].getScanNombre()+uzones[i].getNum_points());
					}
					break;

			}
			}

		}
	}
