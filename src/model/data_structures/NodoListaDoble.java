package model.data_structures;

/**
 * Clase que representa un nodo de una lista doblemente encadenada.
 *
 * @param <E> Tipo del elemento que se almacena en el nodo.
 */
public class NodoListaDoble<E> extends NodoListaSencilla<E>
{
	/**
	 * Constante de serializaci�n
	 */
	private static final long serialVersionUID = 1L;
	
	/**
	 * Nodo anterior.
	 */
	private NodoListaDoble<E> anterior;//TODO 6.2.1 Declara el nodo anterior.
	
	/**
	 * M�todo constructor del nodo doblemente encadenado
	 * @param elemento elemento que se almacenar� en el nodo.
	 */
	public NodoListaDoble(E elemento) 
	{
		super(elemento);//TODO 6.2.2 Asigna el elemento que llega, recuerde herencia.
	}
	
	/**
	 * M�todo que retorna el nodo anterior.
	 * @return Nodo anterior.
	 */
	public NodoListaDoble<E> darAnterior()
	{
		return anterior;
	}
	
	/**
	 * M�todo que cambia el nodo anterior por el que llega como par�metro.
	 * @param anterior Nuevo nodo anterior.
	 */
	
	public void cambiarAnterior(NodoListaDoble<E> anterior)
	{
		this.anterior=anterior;//TODO 6.2.3 Cambia el nodo anterior.
	}
	@Override
	public NodoListaDoble<E> darSiguiente()
	{
		return (NodoListaDoble<E>)siguiente;
	}
}
