package model.data_structures;

public class MaxQueuePQ<Key extends Comparable<Key>> implements IMaxPQ<Key> {
    private int tamanoMax;
	/**
	 * Numero de elementos presentes en el arreglo (de forma compacta desde la posicion 0)
	 */
    private int tamanoAct;
    /**
     * Arreglo de elementos de tamaNo maximo
     */
    public Key elementos[ ];
    
    /**
     * Capacidad inicial por defecto
     */
    private static final int CAPACIDAD_INICIAL_MAXIMA=10;
    
    public MaxQueuePQ()
    { 
    	this(CAPACIDAD_INICIAL_MAXIMA); 
    }
    
    public MaxQueuePQ(int capacity)
    { 
    	elementos = (Key[]) new Comparable[capacity];
	    tamanoMax = capacity;
	    tamanoAct = 0; 
    }
    
    @Override
    public boolean isEmpty() {
        return tamanoAct == 0;
    }

    @Override
    public void insert(Key k) {
    	int i=0;
		if ( tamanoAct == tamanoMax )
        {  // caso de arreglo lleno (aumentar tamaNo)
             tamanoMax = 2 * tamanoMax;
             Key[ ] copia = elementos;
             elementos = (Key[]) new Comparable[tamanoMax];
             for ( i = 0; i < tamanoAct; i++)
             {
              	 elementos[i] = copia[i];
             } 
     	    System.out.println("Arreglo lleno: " + tamanoAct + " - Arreglo duplicado: " + tamanoMax);
        }
		i = tamanoAct-1;
        while (i >= 0 && less(k, elementos[i])){//recorre arreglo hacia atrás mientras el nuevo key sea menor
            elementos[i+1]=elementos[i]; //mueve una posición hacia adelante elementos mayores al key
            i--; //decrementa apuntador i
        }
        elementos[i+1]=k; //asigna k en su correcta posición
        tamanoAct++; //incrementa tamaNo
    }

    @Override
    public Key max() {
    	return elementos[tamanoAct-1];
    }
    
    @Override
    public Key delMax() {
    	Key respuesta=null;
		if(elementos[tamanoAct-1]!=null)
		{
			respuesta=elementos[tamanoAct-1];
			elementos[tamanoAct-1] = null;
			tamanoAct--;
		}	
			
		return respuesta;
    }

    @Override
    public int size() {
        return tamanoAct;
    }
    
    private boolean less (Key v, Key j)
    {
        return v.compareTo(j) < 0;
    }
}
