package model.data_structures;

public interface IListaEnlazada <T>{
	
	boolean esVacia();
	int getTamanio();
	public void insertar(int pIndex, T pItem);
	public void agregarAlFinal (T pItem);
	public void set (int pIndex, T pItem);
	public void eliminar(int pIndex);
	public void clear();
	public Node<T> getNode(int pIndex);
	public Node<T> darRaiz()    ;
	public Node<T> darUltimo();

}
