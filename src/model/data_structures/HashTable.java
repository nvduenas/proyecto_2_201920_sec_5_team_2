package model.data_structures;

public interface HashTable<Key extends Comparable<Key>, Value>{
public TwoWayList<Value> get(Key key);
public void put(Key key, Value val);
public void delete(Key key);
public boolean isEmpty();
public int size();
public boolean contains(Key key);
public int hash(Key key);
public Iterable<Key> keys();
public void resize(int capacity);
public void putInSet(Key key, Value val);

}
