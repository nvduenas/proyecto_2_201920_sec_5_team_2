package model.data_structures;

public interface IRedBlackBST<Key extends Comparable<Key>, Value> {
    public void deleteMin() throws Exception;
    public void deleteMax() throws Exception;
    public void delete(Key key) throws Exception;
    public Node rotateRight(Node h);
    public Node rotateLeft(Node h);
    public void flipColors(Node h);
    public Node moveRedLeft(Node h);
    public Node moveRedRight(Node h);
    public boolean is23();
    public boolean isBalanced();
    public boolean isBST();
    public void keys(Node x, MaxPQ<Key> queue, Key lo, Key hi);
    public Iterable<Key> keys()throws Exception;
    public int rank(Key key);
    public Key select(int k);
    public Key ceiling(Key key) throws Exception;
    public Key floor(Key key) throws Exception;
    public Key max() throws Exception;
    public Key min() throws Exception;
    public int height();
    public Node balance(Node h);
    public void put(Key key, Value val) throws Exception;
    public boolean contains(Key key);
    public Value get(Key key);
    public int size();
    public boolean isRed(Node x);
}
