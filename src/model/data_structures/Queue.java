package model.data_structures;

import java.util.Iterator;
import java.util.NoSuchElementException;

public class Queue<E> implements Iterable<E>{
	private Node<E> first;
	private Node<E> last;
	

	private int size;
	private static class Node<E>{
		private E item;
		private Node<E> next;
	}
	public Queue() {
		first= null;
		last = null;
		size=0;
		
	}
	public Iterator<E> iterator(){
		return new ListIterator(first);

	}
	public class ListIterator implements Iterator<E> {
		private Node<E> current;
		public ListIterator(Node<E> first)
		{
			current=first;
		}
		public boolean hasNext(){ return current!= null;}
		public void remove() {throw new UnsupportedOperationException();}
		public E next(){
			if(!hasNext()) throw new NoSuchElementException();
			E e=current.item;
			current = current.next;
			return e;
		}
	}
	public boolean isEmpty() {
		return first==null;
	}
	public int size() {
		return size;
	}
	public E peek() {
		if(isEmpty()) throw new NoSuchElementException("Queue overflow");
		return first.item;
	}
	public void enqueue (E e) {
		Node oldLast = last;
		last=new Node();
		last.item=e;
		last.next=null;
		if(isEmpty())
		{
			first = last;
			
		}
		else
		{
			oldLast.next=last;
		}
		size++;

		
	}

	public E dequeue() {
		if (isEmpty()) throw new NoSuchElementException("Queue underflow");
		E e = first.item;
		first= first.next;
		size--;
		if(isEmpty()) last=null;
		return e;
		
	}
	
}
