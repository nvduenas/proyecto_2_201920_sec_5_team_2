package model.data_structures;

public class LinearProbingHashST<Key extends Comparable<Key>, Value>implements HashTable<Key,Value> {
    private static final int INIT_CAPACITY=97;

    private int n;
    private int m;
    private Key[] keys;
    private Value[] vals;

    public LinearProbingHashST(int capacity)
    {
        m= capacity;
        n=0;
        keys= (Key[]) new Object[m];
        vals= (Value[]) new Object[m];
    }

    public LinearProbingHashST() {
        this(INIT_CAPACITY);
    }
    @Override
    public int hash(Key key) {
        return (key.hashCode() & 0x7fffffff)% m;
    }

    @Override
    public TwoWayList<Value> get(Key key) {
        if(key==null) throw new IllegalArgumentException("argument to get() is null");
        for(int i=hash(key); keys[i]!=null;i=(i+1)%m)
            if(keys[i].equals(key))
                return (TwoWayList<Value>) vals[i];
            return null;
    }

    @Override
    public void put(Key key, Value val) {
        if(key==null) throw new IllegalArgumentException("first argument to put() is null");
        if(val==null)
        {
            delete(key);
            return;
        }
        if(n>=m/2)
            resize(2*m);
        int i;
        for(i=hash(key);keys[i]!=null;i=(i+1)%m){
            if(keys[i].equals(key)){
                vals[i]=val;
                return;
            }
        }
        keys[i]=key;
        vals[i]=val;
        n++;
    }

    @Override
    public void delete(Key key) {
        if(key==null)throw new IllegalArgumentException("argument to delete() is null");
        if(!contains(key))return;
        int i= hash(key);
        while(!key.equals(keys[i])){
            i=(i+1)%m;
        }
        keys[i]=null;
        vals[i]=null;
        i=(i+1)/m;
        while(keys[i]!=null){
            Key keyToRehash= keys[i];
            Value valTRehash = vals[i];
            keys[i]=null;
            vals[i]=null;
            n--;
            put(keyToRehash, valTRehash);
            i=(i+1)&m;
        }
        n--;
        if(n>0&&n<=m/8) resize(m/2);
        assert check();
    }

    @Override
    public boolean isEmpty() {
        return size()==0;
    }

    @Override
    public int size() {
        return n;
    }

    @Override
    public boolean contains(Key key) {
        if(key==null) throw new IllegalArgumentException("argument to contains() is null");
        return get(key)!=null;
    }

    @Override
    public Iterable<Key> keys() {
        Queue<Key> queue = new Queue<Key>();
        for(int i=0;i<m;i++)
            if(keys[i]!=null)queue.enqueue(keys[i]);
            return (Iterable<Key>) queue;
    }
    public boolean check(){
        if(m<2*n){
            System.err.println("Hash table size m="+m+" array size n ="+n);
            return false;
        }
        for(int i=0;i<m;i++){
            if(keys[i]==null)continue;
            else if(get(keys[i])!= vals[i]){
                System.err.println("get["+keys[i]+"] = "+ get(keys[i])+"; vals[i] = "+ vals[i]);
                return false;
            }
        }
        return false;
    }
    @Override
    public void resize(int capacity) {
        LinearProbingHashST<Key, Value> temp = new LinearProbingHashST<Key, Value>(capacity);
        for(int i=0;i<m;i++){
            if(keys[i]!=null)
            {
                temp.put(keys[i],vals[i]);
            }
        }
        keys=temp.keys;
        vals=temp.vals;
        m=temp.m;
    }

    @Override
    public void putInSet(Key key, Value val) {
        if(contains(key))
        {
            TwoWayList<Value>list=(TwoWayList<Value>)get(key);
            list.add(val);
        }

    }
}
