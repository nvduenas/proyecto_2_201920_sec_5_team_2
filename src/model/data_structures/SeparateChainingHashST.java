package model.data_structures;

import java.util.Iterator;

public class SeparateChainingHashST<Key extends Comparable<Key>, Value > implements HashTable<Key,Value> {
    private static final int INIT_CAPACITY=97;
    private int n;
    private int m;
    private SequentialSearchST<Key, Value>[] st;
    public SeparateChainingHashST(){
        this(INIT_CAPACITY);
    }
    public SeparateChainingHashST(int m){
        this.m=m;
        st = (SequentialSearchST<Key, Value>[]) new SequentialSearchST[m];
        for (int i=0;i<m;i++)
            st[i]=new SequentialSearchST<Key, Value>();
    }
    @Override
    public int hash(Key key) {
        return (key.hashCode() & 0x7fffffff) % m;
    }

    @Override
    public TwoWayList<Value> get(Key key) {
        if (key == null) throw new IllegalArgumentException("argument to get() is null");
        int i = hash(key);
        return st[i].get(key);
    }

    @Override
    public void put(Key key, Value val) {
        if (key == null) throw new IllegalArgumentException("first argument to put() is null");
        if (val == null) {
            delete(key);
            return;
        }

        // double table size if average length of list >= 10
        if (n >= 10*m) resize(2*m);

        int i = hash(key);
        if (!st[i].contains(key)) n++;
        st[i].put(key, val);
    }

    @Override
    public void delete(Key key) {
        if (key == null) throw new IllegalArgumentException("argument to delete() is null");

        int i = hash(key);
        if (st[i].contains(key)) n--;
        st[i].delete(key);

        // halve table size if average length of list <= 2
        if (m > INIT_CAPACITY && n <= 2*m) resize(m/2);
    }

    @Override
    public boolean isEmpty() {
        return size() == 0;
    }

    @Override
    public int size() {
        return n;
    }

    @Override
    public boolean contains(Key key) {
        if (key == null) throw new IllegalArgumentException("argument to contains() is null");
        return get(key) != null;
    }

    @Override
    public Iterable<Key> keys() {
        Queue<Key> queue = new Queue<Key>();
        for (int i = 0; i < m; i++) {
            for (Key key : st[i].keys())
                queue.enqueue(key);
        }
        return (Iterable<Key>) queue;
    }

    @Override
    public void resize(int capacity) {
        SeparateChainingHashST<Key, Value> temp = new SeparateChainingHashST<Key, Value>(capacity);
        for (int i = 0; i < m; i++) {
            for (Key key : st[i].keys()) {
                Iterator<Value>itr =st[i].get(key).iterator();
                Value fn=null;
                while((fn=itr.next())!=null) {
                    temp.put(key, fn);
                }
            }
        }
        this.m  = temp.m;
        this.n  = temp.n;
        this.st = temp.st;
    }

    @Override
    public void putInSet(Key key, Value val) {

    }
}
