package model.data_structures;


import java.util.Comparator;

public class MaxPQ<Key extends Comparable<Key>> implements IMaxPQ<Key> {
    private Key[]pq;
    private boolean comparable;
    private int n;
    private int capacity;
    private Comparator<Key> comparador;
    private static final int CAPACIDAD_INICIAL_MAXIMA=10;
    public void isComparable(Comparator<Key> comparator){

        this.comparador=comparator;
    }
    public void clear()
    {
        pq=(Key[]) new Comparable[CAPACIDAD_INICIAL_MAXIMA];
    }
    public MaxPQ(boolean adaptive)
    {
    	this(CAPACIDAD_INICIAL_MAXIMA,adaptive);
    }

    public MaxPQ(int pCapacity,boolean adaptive)
    {
    	pq=(Key[]) new Comparable[pCapacity]; 
    	capacity=pCapacity;
    	n=0;
    	comparable=adaptive;
    	comparador=new Comparator<Key>() {
            @Override
            public int compare(Key key, Key t1) {
                return 0;
            }
        };
    }
    @Override
    public boolean isEmpty() { return n == 0; }


    @Override
    public void insert(Key k) {
    	int i=0;
		if ( n == capacity-1 && n>5 )
        {  // caso de arreglo lleno (aumentar tamaNo)
             capacity = 2 * n;
             Key[ ] copia = pq;
             pq = (Key[]) new Comparable[capacity];
             for ( i = 0; i < n; i++)
             {
              	 pq[i] = copia[i];
             } 
        }
		
        pq[++n] = k;
        swim(n);
    }

    @Override
    public Key max() {
        Key max=pq[1];
        return max;
    }

    @Override
    public Key delMax() {
        Key max = pq[1];//máximo es 1
        exch(1, n--); //cambiar por último y decremente N
        sink(1);
        pq[n+1] = null;
        return max;
    }

    @Override
    public int size() {
        return n;
    }
    private void sink (int k){
        while (2*k <= n){//Recorrer el arreglo hacia abajo
            int j = 2*k; //j es igual al hijo izq
            if (j < n && less(j, j+1))//selecciona el hijo mayor
                j++;
            if (!less(k, j))//si el padre no es menor, fin
                break;
            exch(k, j); //padre menor, cambiar padre por hijo mayor
            k = j; // bajar al siguiente nivel
        }
    }
    private void swim (int k){
        while (k > 1 && less(k/2, k)){//si es menor el padre
            exch(k, k/2); //intercambie padre e hijo
            k = k/2; //subir al siguiente nivel
        }
    }
    private boolean less (int i, int j)
    {
        if(!comparable&&pq[i]!=null&&pq[j]!=null)
        {
            return pq[i].compareTo(pq[j])<0;
        }
        else {
            return comparador.compare(pq[i],pq[j])<0;
        }
    }
    private void exch(int i, int j)
    {
        Key t = pq[i];
        pq[i] = pq[j];
        pq[j] = t;
    }
}
