package model.data_structures;

import java.util.Iterator;

public class TwoWayList<E> implements Iterable<E> {
	private NodoListaDoble<E>first;
	private NodoListaDoble<E>last;
	private int size;
	public TwoWayList()
	{
		first=new NodoListaDoble<E>(null);
		last=new NodoListaDoble<E>(null);
		size=0;
	}
	public NodoListaDoble<E> darPrimerNodo()
	{
		return first;
	}
	public int size()
	{
		return size;
	}
	public boolean isEmpty()
	{
		if(first==null)
		{
			return true;
		}
		return first.darElemento()==null;
	}
	public void add(E e)
	{
		if(isEmpty())
		{
			first=new NodoListaDoble<E>(e);
			last=first;

		}
		else
		{
			NodoListaDoble<E>oldLast=last;
			last=new NodoListaDoble<E>(e);
			last.cambiarAnterior(oldLast);
			oldLast.cambiarSiguiente(last);
		}
		size++;
	}
	public void add(E e,int i) {
		int count=0;
		NodoListaDoble<E>temp= new NodoListaDoble<E>(e);
		if(isEmpty()&&i==0)
		{
			first=temp;
			last=first;
			size++;
		}
		else if(i-1==size())
		{
			add(e);
			size++;
		}
		else
		{
			NodoListaDoble<E>ref=new NodoListaDoble<E>(e);
			while(ref.darSiguiente()!=null)
			{
				count++;
				if(i==count)
				{
					NodoListaDoble<E>oldNext=ref.darSiguiente().darSiguiente();
					ref.cambiarSiguiente(temp);
					temp.cambiarAnterior(ref);
					oldNext.cambiarAnterior(temp);
					size++;
				}
				ref=ref.darSiguiente();
			}
		}
	}

	
		
	public boolean remove(E e)
	{
		if(isEmpty())
		{
			return false;
		}
		else if(first.darElemento().equals(e))
		{
			NodoListaDoble<E>temp=first;
			first=temp.darSiguiente();
			if(first!=null)
				first.cambiarAnterior(null);
			size--;
			return true;

		}
		else
		{
			NodoListaDoble<E>ref=first;
			while(ref.darSiguiente()!=null)
			{
				if(ref.darSiguiente().darElemento().equals(e))
				{
					System.out.println("Entro 23");
					ref.cambiarSiguiente(ref.darSiguiente().darSiguiente());
					if(ref.darSiguiente()!=null)
					{
						ref.darSiguiente().cambiarAnterior(ref);
					}
					
					size--;
					return true;
					
				}
				ref=ref.darSiguiente();
			}
		}
		return false;
	}
	
		public E get(int index) {
			E ele = null;
			if(index<0||index>=size())
			{
				throw new IndexOutOfBoundsException("Se salio");
			}
			else if(index==0)
			{
				ele=first.darElemento();
			}
			else if (index==size()-1)
			{
				ele=last.darElemento();
			}
			else 		{
				NodoListaDoble<E>actual= first;
				int count=0;
				while(actual.darSiguiente()!=null)
				{
					count++;
					if(count==index&&index>0)
					{
						ele=actual.darSiguiente().darElemento();
					}
					actual=actual.darSiguiente();
				}
			}
			return ele;//TODO 7.1.6 Complete de acuerdo a la documentaci�n
			}
		public boolean contains(Object o)
		{
			System.out.println(indexOf(o));
			return indexOf(o) != -1;
			
		}
		public int indexOf(Object obj) {

		    int index = 0;
		    NodoListaSencilla<E> actual = first;
		    while (actual!=null&&size()>0) {
		        if (actual.darElemento().equals( obj)) {
		            return index;
		        }

		        index++;
		        actual = actual.darSiguiente();
		    }
		    if( obj == null||isEmpty()){
		        return -1;
		    }
		    return -1;
		}
		public void clear() {
			
			first=new NodoListaDoble<E>(null);
			last=first;// TODO Auto-generated method stub
			size=0;
		}

	@Override
	public Iterator<E> iterator() {
		return new Iterator<E>() {
			NodoListaDoble<E> current=first;
			@Override
			public boolean hasNext() {
				return current !=null;
			}

			@Override
			public E next() {
				if (hasNext()){
					E data = current.darElemento();
					current = current.darSiguiente();
					return data;
				}
				return null;
			}
		};
	}
}
