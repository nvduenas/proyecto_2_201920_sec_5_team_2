package model.data_structures;
public interface IMaxPQ<Key extends Comparable<Key>> {

    /**
     * This method return false if the list has at least 1 element
     * @return
     */
    public boolean isEmpty();

    /**
     * This method inserts an element in order
     * @param k
     */
    public void insert(Key k);

    /**
     * This method returns the greater key
     * @return
     */
    public Key max();

    /**
     * This method returns and deletes the grater key
     * @return
     */
    public Key delMax();
    public int size();

}
