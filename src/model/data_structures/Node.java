package model.data_structures;

public class Node<T> {
	T item;
	Node <T> next;
	
	public Node(T pItem,Node<T> pNext )
	{
		item= pItem;
		next = pNext;
	}
	
	public T getItem()
	{
		return item;
	}
	
	public void setItem(T pItem)
	{
		item = pItem;
	}
	
	public Node<T> getNext()
	{
		return next;
	}
	
	public void setNext(Node<T> pNext)
	{
		next = pNext;
	}
}
