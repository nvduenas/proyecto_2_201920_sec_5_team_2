package model.sort;

import model.exception.PersistenciaException;

import java.io.*;


public class Serializador 
{
	/**
	 * Guarda un objeto serializable en un archivo binario
	 * @param ruta ruta donde se crear el archivo con el objeto serializado. ruta != null. Si no existe el archivo con la ruta se crea
	 * @param objeto el objeto que se desea serializar
	 * @throws PersistenciaException Se lanza en caso que no se pueda escribir el archivo
	 */
	public static void guardar(String ruta, Serializable objeto)throws PersistenciaException
	{
		//TODO Complete el m�todo seg�n la documentaci�n
		//	En caso de que no exista el archivo lo debe crear
		// 	En caso que no se pueda escribir el archivo debe generar PersistenciaException
		if(ruta == null){
			throw new PersistenciaException("Ruta vacia ", ruta);
		}
		try {
			FileOutputStream fos = new FileOutputStream(ruta);
			ObjectOutputStream oos = new ObjectOutputStream(fos);
			oos.writeObject(objeto);
			
			oos.flush();
			oos.close();
			fos.close();
				
		} catch (FileNotFoundException e) {
			throw new PersistenciaException(e.getMessage(), ruta);
		}catch (IOException e) {
			throw new PersistenciaException(e.getMessage(), ruta);
		}

	}
	
	/**
	 * Carga un objeto desde un archivo serializado
	 * pre: El archivo solo tiene un objeto
	 * @param ruta la ruta donde se encuentra el archivo que se desea cargar
	 * @param objeto el objeto donde se guadar� la informaci�n cargada. objeto != null
	 * @return en caso que se pueda cargar la informaci�n se guarda en el objeto y se devuelve, de lo contrario solo se devuelve el objeto
	 * @throws PersistenciaException Se lanza en caso que no se pueda leer el archivo o se genere alg�n tipo de error al leerlo
	 */
	@SuppressWarnings("unchecked")
	public static <T> T cargar(String ruta, T objeto)throws PersistenciaException
	{
		try {
			File archivo = new File(ruta);
			if(archivo.exists())
			{
				//TODO Complete el m�todo de acuerdo a la documentaci�n
				//	IMPORTANTE El objeto cargado se debe guardar en el ojeto que llega por par�metro
				
				FileInputStream fis = new FileInputStream(archivo);
				ObjectInputStream ois = new ObjectInputStream(fis);
				
				objeto = (T) ois.readObject();
				
				ois.close();
				fis.close();

			}
			
		} catch (IOException | ClassNotFoundException e) {
		    
			//TODO Complete el m�todo de acuerdo a la documentaci�n
			throw new PersistenciaException(e.getMessage(), ruta);
		}
		return objeto;
	
	}
}
