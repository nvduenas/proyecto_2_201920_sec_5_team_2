package model.sort;

/**
 * Enum que lista los tipos de ordenamiento que se pueden realizar.
 * @author Christian
 *
 */
public enum Ordenamientos 
{
	SHELLSORT,
	MEREGESORT,
	QUICKSORT,
}