package model.sort;


import java.util.Comparator;

public class Ordenador<T extends Comparable<T>>{
	private Comparator<T> comparador;
	public Ordenador(Comparator<T>comparator)
	{
		this.comparador=comparator;
	}
	/**
	 * Ordena los elementos de la lista que llega por par�metro
	 * 
	 * @param algoritmo
	 *            el algoritmo que se desea usar para ordenar. algoritmo != null

	 *            el criterio de comparaci�n que se usar�. comaprador != null
	 * @param elementos
	 *            la lista de elementos que se desea ordenar. elementos != null
	 */
	public void ordenar(Ordenamientos algoritmo, Comparable<T>[] elementos) {
		if(elementos!=null)
		{
			switch (algoritmo)
			{
				case SHELLSORT:
					ordenarShellSort(elementos);
					break;
				case MEREGESORT:
					ordenarMergeSort(elementos);
					break;
				case QUICKSORT:
					ordenarQuickSort(elementos);
					break;
			}
		}

	}
	private  void quickSort(Comparable<T>[] a, int lo, int hi) {
		if (hi <= lo) return;
		int j = partition(a, lo, hi);
		quickSort(a, lo, j-1); // Sort left part a[lo .. j-1].
		
		
	}
	private void ordenarQuickSort(Comparable<T>[] a) {
		// TODO Auto-generated method stub
		quickSort(a, 0,a.length-1);
	}
	private  int partition(Comparable<T>[] a, int lo, int hi){
		
		int i = lo, j = hi+1;
		Comparable<T> piv = a[lo];
		while (true){
		while (less(a[++i], piv))
		if (i == hi) break; 
		while (less(piv, a[--j]))
		if (j == lo) break; 
		if (i >= j) break; 
		exch(a, i, j);
		}
		exch(a, lo, j); 
		return j; 
		}
	@SuppressWarnings("unchecked")
	private void ordenarMergeSort(Comparable<T>[] element) {
		Comparable<T>[]aux=new Comparable[element.length];
		mergeSort(aux, element, 0,element.length-1);
	}


	private void mergeSort(Comparable<T>[] aux, Comparable<T>[] elementos, int lo, int hi)
	{
		if(hi<=lo)
			return;
		int mid = lo + (hi-lo)/2;
		mergeSort(aux, elementos, lo, mid);
		mergeSort(aux, elementos, mid+1,hi);
		merge(aux, elementos, lo,hi,mid);
	}
	public void merge(Comparable<T>[] aux, Comparable<T>[] elementos, int lo, int hi, int mid)
	{

		int k=0, i=lo, j=mid+1;
		for(k=lo; k<=hi; k++)
		{
			aux[k]=elementos[k];
		}
		for(k=lo; k<=hi; k++)
		{
			if(i>mid) 
				elementos[k]=aux[j++];
			else if(j>hi)
				elementos[k]=aux[i++];
			else if(less(aux[j],aux[i]))
				elementos[k]=aux[j++];
			else
				elementos[k]=aux[i++];
		}
	}




	private void ordenarShellSort(Comparable<T>[] elementos) {
		// TODO Auto-generated method stub
		int h=1;
		while(h<elementos.length/3)
			h=h*3+1;
		while(h>=1)
		{
			for(int i=h; i<elementos.length;i++)
			{
				for (int j=i;j>=h&&less(elementos[j],elementos[j-h]);j-=h)
				{
					exch(elementos, j, j-h);
				}
				h=h/3;
			}
		}
	}



	@SuppressWarnings("rawtypes")
	public  void exch(Comparable<T>[] a, int i,int j)
	{
		Comparable<T> t=a[i];
		a[i]=a[j];
		a[j]=t;
	}
	@SuppressWarnings({ "rawtypes", "unchecked" })
	private  boolean less(Comparable v, Comparable w)
	{
		return comparador.compare((T)v,(T)w)<0;
	}
}
	