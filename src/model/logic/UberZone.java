package model.logic;

import java.util.Comparator;

public class UberZone implements Comparable<UberZone> {
    private int mvid;
    private String scanNombre;
    private double shape_leng;
    private double shape_area;
    private int num_points;
    public UberZone(int mvid,String scanNombre,double shape_leng,double shape_area,int num_points){
        this.mvid=mvid;this.scanNombre=scanNombre;this.shape_leng=shape_leng;this.shape_area=shape_area;this.num_points=num_points; }
    public int getMvid() { return mvid; }
    public String getScanNombre() { return scanNombre; }
    public double getShape_leng() { return shape_leng; }
    public double getShape_area() { return shape_area; }
    public int getNum_points() { return num_points; }

    static Comparator<UberZone> comparatorNumPoints()
    {
        return new Comparator<UberZone>() {
            @Override
            public int compare(UberZone t1, UberZone t2) {
                return t1.num_points>t2.num_points?1:t1.num_points<t2.num_points?-1:0;
            }
        };
    }
    public double darLongitud() {
        return (shape_area)/(shape_leng);
    }

    @Override
    public int compareTo(UberZone uberZone) {
        return 0;
    }
}
