package model.logic;

public class Malla implements Comparable<Malla>{
	private int id;
	private double longitud;
	private double latitud;
	public Malla(int pId, double pLongitud, double pLatitud){
		id=pId;
		longitud=pLongitud;
		latitud=pLatitud;
	}
	public int darId(){
		return id;
		
	}
	public double darLongitud(){
		return longitud;
		
	}
	public double darLatitud(){
		return latitud;
		
	}

	@Override
	public int compareTo(Malla malla) {
		return this.latitud>malla.latitud?1:this.latitud==malla.latitud?0:-1;
	}
}
