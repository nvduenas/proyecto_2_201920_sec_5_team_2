package model.logic;

import java.util.Comparator;

public class UBERTrip implements Comparable<UBERTrip>{
	private  int trimester;
	private int sourceid;
	private int dstid;
	private int date;
	private double mean_travel_time;
	private double standard_deviation_travel_time;
	private double geometric_mean_travel_time;
	private double geometric_standard_deaviation_time;
	public UBERTrip(int trimester,int sourceid, int dstid, int date, double mean_travel_time, double standard_deviation_travel_time,
                    double geometric_mean_travel_time, double geometric_standard_deaviation_time)
	{
		this.trimester=trimester;
		this.sourceid= sourceid;
		this.dstid= dstid;
		this.date = date;
		this.mean_travel_time=mean_travel_time;
		this.standard_deviation_travel_time = standard_deviation_travel_time;
		this.geometric_mean_travel_time= geometric_mean_travel_time;
		this.geometric_standard_deaviation_time=geometric_standard_deaviation_time;
	}
	public int getTrimester()
	{
		return trimester;
	}
	public int give_sourceid()
	{
		return sourceid;
	}
	public int givedstid() { return dstid; }
	public int give_date()
	{
		return date;
	}
	public double give_mean_travel_time()
	{
		return mean_travel_time;
	}
	public double give_standard_deviation_travel_time()
	{
		return standard_deviation_travel_time;
	}
	public double give_geometric_mean_travel_time()
	{
		return geometric_mean_travel_time;
	}
	public double give_geometric_standard_deaviation_time()
	{
		return geometric_standard_deaviation_time;
	}
	@Override
	public int compareTo(UBERTrip u1) {
			if(this.standard_deviation_travel_time>u1.standard_deviation_travel_time){return 1;}
			else if(this.standard_deviation_travel_time<u1.standard_deviation_travel_time){return -1;}
			else return 0;
	}
	static Comparator<UBERTrip> comparadorPorTiempo()
	{
		return new Comparator<UBERTrip>() {
			@Override
			public int compare(UBERTrip uberTrip, UBERTrip t1) {
				if(uberTrip.give_mean_travel_time()>t1.give_mean_travel_time())// TODO Auto-generated method stub
					return 1;
				else if(uberTrip.give_mean_travel_time()<t1.give_mean_travel_time())
					return -1;
				else// TODO Auto-generated method stub
					if(uberTrip.standard_deviation_travel_time>t1.standard_deviation_travel_time){return 1;}
					else if(uberTrip.standard_deviation_travel_time<t1.standard_deviation_travel_time){return -1;}
					else return 0;
			}
		};
	}
	static Comparator<UBERTrip> comparadorPorZona()
	{
		return new Comparator<UBERTrip>() {
			@Override
			public int compare(UBERTrip uberTrip, UBERTrip t1) {
				int menor1=uberTrip.givedstid();
				int menor2=t1.givedstid();

				if(uberTrip.give_sourceid()>uberTrip.givedstid()){
					menor1=uberTrip.givedstid();
				}
				if(t1.give_sourceid()>t1.givedstid()){
					menor2=t1.givedstid();
				}
				if(menor1>menor2)// TODO Auto-generated method stub
					return 1;
				else if(menor1<menor2)
					return -1;
				else
					return 0;
			}
		};
	}
	static Comparator<UBERTrip> comparadorPorDate()
	{
		return new Comparator<UBERTrip>() {
			@Override
			public int compare(UBERTrip uberTrip, UBERTrip t1) {

				if(uberTrip.give_date()>t1.give_date())// TODO Auto-generated method stub
					return 1;
				else if(uberTrip.give_date()>t1.give_date())
					return -1;
				else
					return 0;
			}
		};
	}
}
