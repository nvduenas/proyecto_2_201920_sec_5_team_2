package model.logic;

import java.util.Comparator;

public class TravelTime implements Comparable<TravelTime>{
	private int trimester;
	private int sourceid;
	private int distid;
	private int hod;
	private double mean_travel_time;
	private double standard_deviation_travel_time;

	public TravelTime(int trimester, int sourceid, int distid, int hod, double mean_travel_time, double standard_deviation_travel_time)
	{   
		this.trimester=trimester;
		this.sourceid= sourceid;
		this.distid= distid;
		this.hod = hod;
		this.mean_travel_time=mean_travel_time;
		this.standard_deviation_travel_time = standard_deviation_travel_time;
	}
	
	public int  give_Trimester() {
		return trimester;
	}
	
	public int give_sourceid()
	{
		return sourceid;
	}
	
	public int give_distid() { 
		return distid; 
	}
	
	public int give_hod() { 
		return hod; 
	}
	
	public double give_mean_travel_time()
	{
		return mean_travel_time;
	}
	
	public double give_standard_deviation_travel_time() {
		return standard_deviation_travel_time; 
	}
	
	@Override
	public int compareTo(TravelTime tT) {
		if(this.mean_travel_time>tT.mean_travel_time)
			return 1;
		else if(this.mean_travel_time<tT.mean_travel_time)
			return -1;
		else
			return 0;
	}
	static Comparator<TravelTime> comparadorPorHora() {
		return new Comparator<TravelTime>() {
			@Override
			public int compare(TravelTime t1, TravelTime t2) {
				return t1.give_hod() > t2.give_hod() ? 1 : t1.give_hod() < t2.give_hod() ? -1 : 0;
			}
		};
	}
}
