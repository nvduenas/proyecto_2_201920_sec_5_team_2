package model.logic;

import com.opencsv.CSVReader;
import model.data_structures.*;
import model.sort.Ordenador;

import java.io.*;
import java.text.DecimalFormat;
import java.util.Iterator;

import com.google.gson.*;

/**
 * Definicion del modelo del mundo
 *
 */
public class MVCModelo 
{


	private int menor;
	private int mayor;
	public static final String RUTA_NODOS="./data/nodes_of_red_vial-wgs84_shp.csv";
	public static final String RUTA_JSON="./data/bogota_cadastral.json";
    private TwoWayList<TravelTime> viajesHourly;
    private TwoWayList<UBERTrip> tripsMonthly;
    private TwoWayList<UberZone> zoneList;
    private TwoWayList<Malla> mallaList;
    private FCollection fCollection;
	private Ordenador<UBERTrip> ordenadorTravelTime;
	private Ordenador<UBERTrip> ordenadorSourceId;
	private SeparateChainingHashST<Double,Malla>hashST;
    private SeparateChainingHashST<Double,UberZone>hashST1;
	public MVCModelo()
	{
	    hashST1=new SeparateChainingHashST<>();
	    tripsMonthly=new TwoWayList<UBERTrip>();
	    hashST=new SeparateChainingHashST<>();
	    zoneList=new TwoWayList<UberZone>();
        viajesHourly = new TwoWayList<TravelTime>();
        mallaList= new TwoWayList<Malla>();
		ordenadorTravelTime =new Ordenador<UBERTrip>(UBERTrip.comparadorPorTiempo());
		ordenadorSourceId=new Ordenador<UBERTrip>(UBERTrip.comparadorPorZona());
		fCollection=new FCollection();
	}
	public MVCModelo(int f)
    { try { load(); } catch (Exception e) { e.printStackTrace(); } }
	public int darMenor() { return menor; }
    public int darMayor(){ return mayor; }
    @SuppressWarnings("resource")
	public void load() throws IOException{
        menor=9999;
        mayor=0;
        int i=1;
        while(i<3)
        {
            FileInputStream fs= new FileInputStream(darRutaFile(i)[0]);
            InputStreamReader sr= new InputStreamReader(fs);
            BufferedReader br = new BufferedReader(sr);
            CSVReader cv= new CSVReader(br);
            if (cv.readNext().length==7) {
                String[] cargarL = new String[7];
                while ((cargarL = cv.readNext()) != null) {
                    int sourceid = Integer.parseInt(cargarL[0]);
                    int dstid = Integer.parseInt(cargarL[1]);
                    int hour = Integer.parseInt(cargarL[2]);
                    double mean_travel_time = Double.parseDouble(cargarL[3]);
                    double standard_deviation_travel_time = Double.parseDouble(cargarL[4]);
                    double geometric_mean_travel_time = Double.parseDouble(cargarL[5]);
                    double geometric_standard_deviation_time = Double.parseDouble(cargarL[6]);
                    if(menor>sourceid) { menor= sourceid;}
                    if(menor>dstid) {menor=dstid;}
                    if(mayor<sourceid) {mayor=sourceid;}
                    if (mayor<dstid) {mayor=dstid;}
                    TravelTime temp= new TravelTime(i,sourceid,dstid,hour,mean_travel_time,standard_deviation_travel_time);
                    viajesHourly.add(temp);
                }
            }
            fs.close();
            br.close();
            cv.close();
            i++;
        }
        System.out.println("Hourly ");
        /**
        int k=1;
        while(k<3)
        {
            FileInputStream fs1= new FileInputStream(darRutaFile(k)[1]);
            InputStreamReader src= new InputStreamReader(fs1);
            BufferedReader bff = new BufferedReader(src);
            CSVReader cvs= new CSVReader(bff);
            if (cvs.readNext().length==7) {
                String[] cargar = new String[7];
                while ((cargar = cvs.readNext()) != null) {
                    int sourceid = Integer.parseInt(cargar[0]);
                    int dstid = Integer.parseInt(cargar[1]);
                    int month = Integer.parseInt(cargar[2]);
                    double mean_travel_time = Double.parseDouble(cargar[3]);
                    double standard_deviation_travel_time = Double.parseDouble(cargar[4]);
                    double geometric_mean_travel_time = Double.parseDouble(cargar[5]);
                    double geometric_standard_deviation_time = Double.parseDouble(cargar[6]);
                    UBERTrip temp=new UBERTrip(k,sourceid,dstid,month,mean_travel_time,standard_deviation_travel_time,geometric_mean_travel_time,geometric_standard_deviation_time);
                    tripsMonthly.add(temp);
                }
            }
            fs1.close();
            bff.close();
            cvs.close();
            k++;
        }
         **/
        System.out.println("Monthly");
		Gson gson=new Gson();
        System.out.println("Process terminated----------------\nStart loading json File----------------");
        loadJson();
        Feature[]features=fCollection.features;
        int j=0;

        for (Feature feature:features) {
            int moveId = feature.properties.MOVEMENT_ID;
            String scaName = feature.properties.scanombre;
            double shape_area=feature.properties.shape_area;
            double shape_leng=feature.properties.shape_leng;
            int getNum=feature.geometry.coordinates.length;
            UberZone temp=new UberZone(moveId,scaName,shape_leng,shape_area,getNum);
            zoneList.add(temp);
        }
        for (Feature feature:features) {
            System.out.println(feature.properties.scanombre);
        }
        System.out.println("Process terminated, number of zones: "+ zoneList.size()+"----------------\nStart loading nodes File----------------");
        loadNodes();
	}
	public void loadJson() throws FileNotFoundException {
        FileInputStream inputStream = new FileInputStream(RUTA_JSON);
        InputStreamReader inputStreamReader = new InputStreamReader(inputStream);
        BufferedReader bufferedReader = new BufferedReader(inputStreamReader);
        FCollection g = new Gson().fromJson(bufferedReader, FCollection.class);
        fCollection=g;
    }
    public void loadNodes() throws IOException {
        FileInputStream inputStream = new FileInputStream(RUTA_NODOS);
        InputStreamReader inputStreamReader = new InputStreamReader(inputStream);
        BufferedReader bufferedReader = new BufferedReader(inputStreamReader);
        CSVReader cvsR=new CSVReader(bufferedReader);

        String[]load=new String[3];
        while ((load = cvsR.readNext()) != null){
            int id=Integer.parseInt(load[0]);
            double longitud=Double.parseDouble(load[1]);
            double latitud=Double.parseDouble(load[2]);
            Malla temp=new Malla(id,longitud,latitud);
            mallaList.add(temp);
        }
        System.out.println("Nodes loaded, Length: "+mallaList.size());
	}
    class Properties {
    long cartodb_id;
    String scacodigo;
    long scatipo;
    String scanombre;
    double shape_leng;
    double shape_area;
    int MOVEMENT_ID;
    String DISPLAY_NAME;}
    class GeometryData {
    String type;
    double[][][][] coordinates;    }
    class Feature {
    String type;
    GeometryData geometry;
    Properties properties;}
    class FCollection {
    String type;
    Feature[] features;}

	public String[]darRutaFile(int i)
    {
        String rutaHourly="./data/bogota-cadastral-2018-"+i+"-All-HourlyAggregate.csv";
        String rutaMontly="./data/bogota-cadastral-2018-"+i+"-All-MonthlyAggregate.csv";
        String[]ruta=new String[2];
        ruta[0]=rutaHourly;
        ruta[1]=rutaMontly;
        return ruta; }


    public TwoWayList<TravelTime> darViajesHourly() { return viajesHourly; }

	public Malla[] giveNorthZones(int n)
    {
        Malla[]uz=new Malla[n];
        MaxPQ<Malla>pq=new MaxPQ<Malla>(false);
        Iterator<Malla>itr=mallaList.iterator();
        Malla mll=null;
        while((mll=itr.next())!=null)
        {
            pq.insert(mll);
        }
         for(int i=0;i<n;i++)
         {
             uz[i]=pq.delMax();
         }
         pq.clear();
         return uz;
    }
    class Key1{
	    private double latitude;
	    private double longitude;
	    public Key1(double latitude, double longitude)
        {
            this.latitude=latitude;
            this.longitude=longitude;
        }
        public double gen_key()
        {
            return longitude*13+57*latitude;
        }
    }
    public double[] aprrox2(double d, double j)
    {
        double[]data=new double[2];
        DecimalFormat df = new DecimalFormat("#,##");
        double longi=Double.parseDouble(df.format(d));
        double lati= Double.parseDouble(df.format(j));
        data[0]=longi;
        data[1]=lati;
        return data;
    }
    public TwoWayList<Malla> neighborLocations(double latitude, double longitude)
    {
        TwoWayList<Malla>re=new TwoWayList<Malla>();
        double[]l=aprrox2(longitude,latitude);
        Key1 primeKey=new Key1(l[0], l[1]);
        Iterator<Malla>itr=mallaList.iterator();

        Malla malla=null;
        while((malla=itr.next())!=null)
        {
            double[]l1=aprrox2(malla.darLongitud(),malla.darLatitud());
            Key1 key=new Key1(l1[0], l1[1]);
            hashST.put(key.gen_key(),malla);
        }
        if(!hashST.contains(primeKey.gen_key()))
        {
            new Exception("The list doesnt contanins zones with those parameters");
        }
        else{
            TwoWayList<Malla>lf=hashST.get(primeKey.gen_key());
            Iterator<Malla>it=lf.iterator();
            Malla imalla=null;
            while ((imalla=it.next())!=null)
            {
                if(imalla.darLongitud()!=longitude||imalla.darLatitud()!=latitude)
                {
                    re.add(imalla);
                }
            }
            lf.clear();
        }
        hashST=new SeparateChainingHashST<Double, Malla>();
        return re;
    }
    public TwoWayList<UBERTrip> limStdDev(double floor,double roof)
    {
        TwoWayList<UBERTrip>ub=new TwoWayList<UBERTrip>();
        RedBlackBST<String,UBERTrip>tree=new RedBlackBST<String,UBERTrip>();
        Iterator<UBERTrip>itr=tripsMonthly.iterator();
        UBERTrip tripsy=null;
        while((tripsy=itr.next())!=null)
        {
            if(tripsy.getTrimester()==1&&tripsy.give_standard_deviation_travel_time()>floor&&tripsy.give_standard_deviation_travel_time()<=roof)
            {
                tree.put(tripsy.give_standard_deviation_travel_time()+"-"+tripsy.give_sourceid()+"-"+tripsy.givedstid(),tripsy);
            }
        }
        UBERTrip max=null;
        while((max=tree.get(tree.max()))!=null)
        {
            tree.deleteMax();
            ub.add(max);
        }
        return ub;
    }
    class Key2 implements Comparable<Key2> {
	    private int distid;
	    private int  hour;
	    public Key2(int  distid,int hour)
        {
            this.distid=distid;
            this.hour=hour;
        }
        public int genKey()
        {
            return distid*13+hour*57;
        }

        @Override
        public int compareTo(Key2 key2) {
            return this.hour+this.distid>key2.hour+key2.distid?1:this.hour+this.distid<key2.hour+key2.distid?-1:0;
        }
    }
    public TwoWayList<TravelTime> getTravelTimesByGivenZoneHour(int distid,int hour)
    {
        Key2 primeKey=new Key2(distid,hour);
        SeparateChainingHashST<Integer,TravelTime>hashTimes=new SeparateChainingHashST<Integer,TravelTime>();
        Iterator<TravelTime>itr=viajesHourly.iterator();
        TravelTime br=null;
        while((br=itr.next())!=null)
        {
            Key2 key=new Key2(br.give_distid(),br.give_hod());
            hashTimes.put(key.genKey(),br);
        }
        return hashTimes.get(primeKey.genKey());
    }
    public TravelTime[] getTravelTimesByGivenZoneRangeHour(int distid, int floor, int roof)
    {
        SeparateChainingHashST<Integer,TravelTime>hashTimes=new SeparateChainingHashST<Integer,TravelTime>();
        Iterator<TravelTime>itr=viajesHourly.iterator();
        TravelTime br=null;
        while((br=itr.next())!=null)
        {
            if(br.give_hod()>floor&&br.give_hod()<=roof)
            {
                hashTimes.put(distid,br);
            }
        }
        MaxPQ<TravelTime>travelPQ=new MaxPQ<>(true);
        travelPQ.isComparable(TravelTime.comparadorPorHora());
        TwoWayList<TravelTime>tl=hashTimes.get(distid);
        Iterator<TravelTime>it=tl.iterator();
        TravelTime tempo= null;
        while((tempo=it.next())!=null)
        {
            travelPQ.insert(tempo);
        }
        TravelTime[]lr=new TravelTime[travelPQ.size()];

        for (int i = 0; i < lr.length; i++) {
            lr[i]=travelPQ.delMax();
        }

        return lr;
    }
    public UberZone[] priorityZonesByNum(int n)
    {
        UberZone[] uz= new UberZone[n];
        MaxPQ<UberZone>mp= new MaxPQ<>(true);
        mp.isComparable(UberZone.comparatorNumPoints());
        Iterator<UberZone>itr=zoneList.iterator();
        UberZone ubz=null;
        while((ubz=itr.next())!=null)
        {
            mp.insert(ubz);
        }
        for(int i=0;i<n;i++)
        {
            uz[i]=mp.delMax();
        }
        return uz;
    }
    /**
     * Obtener las N letras más frecuentes por las que comienza el nombre de una zona (No
     *	diferenciar las mayúsculas de las minúsculas). N es un dato de entrada.
     *	El resultado debe aparecer de mayor a menor. Para cada letra se debe imprimir la letra y el
     *nombre de las zonas que comienzan por esa letra.
     * @param N
     * @return
     */
    public MaxPQ givenMasFrecuentesLetra(int N){
        MaxPQ zonasPQ=new MaxPQ(false);

        MaxPQ masFrecuentes=new MaxPQ<>(false);
        //Contadores para cada letra inicial
        int a=0; int b=0; int c=0; int d=0; int e=0; int f=0; int g=0; int h=0; int i=0; int j=0; int k=0; int l=0; int m=0;
        int n=0; int o=0; int p=0; int q=0; int r=0; int s=0; int t=0; int u=0; int v=0; int w=0;int x=0; int y=0; int z=0;
        Iterator<UberZone>itr=zoneList.iterator();
        UberZone mll=itr.next();
        String act=itr.toString();;
        String firstLetter=String.valueOf(act.charAt(0));
        while(itr!=null)
        {
            if(firstLetter.equals("a")){a++;};
            if(firstLetter.equals("b")){b++;};
            if(firstLetter.equals("c")){c++;};
            if(firstLetter.equals("d")){d++;};
            if(firstLetter.equals("e")){e++;};
            if(firstLetter.equals("f")){f++;};
            if(firstLetter.equals("g")){g++;};
            if(firstLetter.equals("h")){h++;};
            if(firstLetter.equals("i")){i++;};
            if(firstLetter.equals("j")){j++;};
            if(firstLetter.equals("k")){k++;};
            if(firstLetter.equals("l")){l++;};
            if(firstLetter.equals("m")){m++;};
            if(firstLetter.equals("n")){n++;};
            if(firstLetter.equals("o")){o++;};
            if(firstLetter.equals("p")){p++;};
            if(firstLetter.equals("q")){q++;};
            if(firstLetter.equals("r")){r++;};
            if(firstLetter.equals("s")){s++;};
            if(firstLetter.equals("t")){t++;};
            if(firstLetter.equals("u")){u++;};
            if(firstLetter.equals("v")){v++;};
            if(firstLetter.equals("w")){w++;};
            if(firstLetter.equals("x")){x++;};
            if(firstLetter.equals("y")){y++;};
            if(firstLetter.equals("z")){z++;};
        }
        //agregar los contadores de cada letra al MaxPQ
        zonasPQ.insert(a);zonasPQ.insert(b);zonasPQ.insert(c);zonasPQ.insert(d);
        zonasPQ.insert(e);zonasPQ.insert(f);zonasPQ.insert(g); zonasPQ.insert(h);
        zonasPQ.insert(i);zonasPQ.insert(j);zonasPQ.insert(k);zonasPQ.insert(l);
        zonasPQ.insert(m);zonasPQ.insert(n);zonasPQ.insert(o);zonasPQ.insert(p);
        zonasPQ.insert(q);zonasPQ.insert(r);zonasPQ.insert(s);zonasPQ.insert(t);
        zonasPQ.insert(u);zonasPQ.insert(v);zonasPQ.insert(w);zonasPQ.insert(x);
        zonasPQ.insert(y);zonasPQ.insert(z);
        //agregar los mayores, N cantidad de veces
        for (int z2 = 0; z2 < N; z2++) {
            masFrecuentes.insert((String) zonasPQ.max());
        }
        return masFrecuentes;
    }


  class Key3{
    private double latitude;
    private double longitude;
    public Key3(double latitude, double longitude)
    {
        this.latitude=latitude;
        this.longitude=longitude;
    }
    public double gen_key()
    {
        return longitude*13+57*latitude;
    }
}

    public TwoWayList<UberZone> DosA(double latitude, double longitude)
    {

        TwoWayList<UberZone>re=new TwoWayList<UberZone>();
        double[]l=aprrox2(longitude,latitude);
        Key3 primeKey=new Key3(l[0], l[1]);
        Iterator<UberZone>itr=zoneList.iterator();

        UberZone zone=null;
        while((zone=itr.next())!=null)
        {

            DecimalFormat df = new DecimalFormat("#.##");
            double[]l1=aprrox2(zone.darLongitud(),zone.getShape_leng());
            Key3 key=new Key3(l1[0], l1[1]);
            hashST1.put(key.gen_key(),zone);
        }
        if(!hashST1.contains(primeKey.gen_key()))
        {
            new Exception("The list doesnt contanins zones with those parameters");
        }
        else{
            TwoWayList<UberZone>lf=hashST1.get(primeKey.gen_key());
            Iterator<UberZone>it=lf.iterator();
            UberZone izone=null;
            while ((izone=it.next())!=null)
            {
                if(izone.darLongitud()!=longitude||izone.getShape_leng()!=latitude)
                {
                    re.add(izone);
                }
            }
            lf.clear();
        }
        return re;
    }
    public TwoWayList<UBERTrip> tresA(double lim1,double lim2)
    {
        TwoWayList<UBERTrip>uber=new TwoWayList<UBERTrip>();
        RedBlackBST<String,UBERTrip>tree=new RedBlackBST<String,UBERTrip>();
        Iterator<UBERTrip>itr=tripsMonthly.iterator();
        UBERTrip trip=null;
        while((trip=itr.next())!=null)
        {
            if(trip.give_mean_travel_time()>lim1&&trip.give_mean_travel_time()<=lim2&&trip.getTrimester()==1)
            {
                tree.put("origen:" +trip.give_sourceid()+ "destino"+trip.givedstid()+"mes"+trip.give_date()+"tiempo promedio"+trip.give_mean_travel_time(), trip);
            }
        }
        UBERTrip max=null;
        while((max=tree.get(tree.max()))!=null)
        {
            tree.deleteMax();
            uber.add(max);
        }
        return uber;
    }
    public void tablaAscii(){
        int total=0;
        int missing=0;
        int times=0;
        MaxPQ zt=new MaxPQ<>(false);
        total=24*zoneList.size()*4;
        {
            for (int j = 0; j < tripsMonthly.size(); j++) {
                if(tripsMonthly.get(j).give_sourceid()!=tripsMonthly.get(j-1).give_sourceid()){
                    zt.insert(tripsMonthly.get(j).give_sourceid());
                }
            }
        }
        for (int i = 0; i < zoneList.size(); i++) {
            missing=total-zt.size();
            System.out.println(zoneList.get(i).getMvid()+" | "+missing + "/n");
        }

    }
}