package model.exception;

import java.io.File;
import java.io.FileNotFoundException;
import java.io.FileOutputStream;
import java.io.PrintWriter;

@SuppressWarnings("serial")
public class ExcepcionGeneral extends Exception
{
	public static final String ARCHIVO_LOG_ERRORES="data/log/error,log";
	private String mensaje;
	public ExcepcionGeneral(String mensaje)
	{
		super();
		this.mensaje=mensaje;
	}
	public String darNombreError() 
	{
		return "Excepcion general";
	}
	public  String darMensajeError()
	{
		return mensaje;
	}
	public void escribirLog(String mensaje)
	{
		try {
			FileOutputStream temp = new FileOutputStream(new File(ARCHIVO_LOG_ERRORES));
			PrintWriter hola = new PrintWriter(temp);
			hola.write(darMensajeError());
			hola.close();
		
		} catch (FileNotFoundException e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		}
	}
	
}
